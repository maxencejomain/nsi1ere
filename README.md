# NSI1ere

Les ressources pour la spé NSI de 1ère - lycée du Forez - par M. Lémery

## Utilisation
En cliquant sur les fichiers ipnyb on accède aux notebooks. Il s'agit de fichier combinant des blocs de texte et des extraits de code python.

Il est possible de les télécharger en cliquant sur le bouton `Download` :  

![HowToDownload](img/HowToDownload.png)

## Python au lycée
Au lycée l'environnement de développement (IDE) fourni avec edupython est pyscripter :
![pyscripter](https://upload.wikimedia.org/wikipedia/commons/a/ad/Console_PyScripter.png)
Sur la partie haute, on peut écrire des programmes qu'on execute avec le bouton "executer".  
Sur la partie basse, dans la console, on peut écrire directement des commandes pour les tester. C'est également là que le moteur python fait les affichages :
* des messages d'erreur
* de la commande print  

Pour reprendre les exercices fait en classe à la maison, il est possible d'utiliser le même (IDE) en téléchargeant la version 3.0 sur la page d'[edupython](https://edupython.tuxfamily.org/#t%C3%A9l%C3%A9chargement)

### ouvrir et éditer des notebook
Pour ouvrir et éditer des notebook, comme celui-ci, on utilise *Jupyter* accessible dans *pyscripter* dans le sous-menu **Outils** du menu **Outils**